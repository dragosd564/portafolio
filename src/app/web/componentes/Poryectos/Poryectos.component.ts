import { Component, OnInit } from '@angular/core';
import datos from "src/data/datosProyectos.json";

interface proyectos {
  img: string;
  nombre: string;
  descripcion: string;
  lenguaje: string;
}
@Component({
  selector: 'app-Poryectos',
  templateUrl: './Poryectos.component.html',
  styleUrls: ['./Poryectos.component.css']
})
export class PoryectosComponent implements OnInit {
  ModelProyectos: proyectos[] = datos;
  constructor() { }

  ngOnInit() {
  }

}
