import { Component, OnInit } from '@angular/core';
import data from "src/data/conocimiento.json";
import datos from 'src/data/estadistica.json';

interface datos {
  img: string;
  titulo: string;
  nivel: string;

}
interface Estadistica {
  conocimento: string;
  manejoLenguaje: string;
}
@Component({
  selector: 'app-Miconocimiento',
  templateUrl: './Miconocimiento.component.html',
  styleUrls: ['./Miconocimiento.component.css']
})
export class MiconocimientoComponent implements OnInit {
  conocimiento: datos[] = data;
  Porcentajes: Estadistica[] = datos;
  constructor() { }

  ngOnInit() {
  }

}
