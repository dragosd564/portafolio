import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
//Variables de animaciones
A_lightSpeedInLeft :string= "animate__animated animate__lightSpeedInLeft";
A_bounceInDown     :string= "animate__animated animate__bounceInDown";
A_fadeIn  :string= "animate__animated animate__fadeIn";
A_fadeInUp :string= "animate__animated animate__fadeInUp";
  constructor() { }

  ngOnInit() {
  }

}
