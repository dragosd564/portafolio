import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//component
import { WebComponent } from './web.component';
import { BannerComponent } from './componentes/banner/banner.component';
import { FooterComponent } from './componentes/footer/footer.component';
import { MiconocimientoComponent } from './componentes/Miconocimiento/Miconocimiento.component';
import { HeaderComponent } from './componentes/header/header.component';
import { SeccionSobreMiComponent } from './componentes/seccionSobreMi/seccionSobreMi.component';
import { PoryectosComponent } from './componentes/Poryectos/Poryectos.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    WebComponent,
    BannerComponent,
    FooterComponent,
    MiconocimientoComponent,
    HeaderComponent,
    SeccionSobreMiComponent,
    PoryectosComponent
  ]
})
export class WebModule { }
